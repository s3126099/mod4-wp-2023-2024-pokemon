
let tableParentId = 'table';
let detailsParentId = 'details';
let trainers = {}

let selectedTrainerID;
let selectedDetails = false;

function getRowId(resource) {
    return `${resource.id}_row`
}

function clearTrainerDetails() {
    const parent = document.getElementById(detailsParentId);
    parent.innerHTML = '';
}

function updateDetails(trainerId) {
    const tr = trainers?.data?.find(item => item.id === trainerId);
    const parent = document.getElementById(detailsParentId);
    selectedTrainerID = tr.id;
    selectedDetails = true;

    parent.innerHTML = `
    <div class="card" id="${tr.id}_card">
        ${tr.profileUrl ? `<img src="${tr.profileUrl}" class="card-img-top" alt="${tr.name}">` : ''}
      <div class="card-body">
        <h5 class="card-title">${tr.name}</h5>
        <p class="card-text">
            Creation Date: ${tr.created} <br>
            Last Updated: ${tr.lastUpDate}
        </p>
      </div>
    </div>
    <div class="card" id="${tr.id}_card_update" style="padding: 20px">
        <form id="updateCardForm">
            <div class="form-group">
                <label for="newTrainerInput">Update Trainer Name</label>
                <input type="text" class="form-control" id="updateTrainerInput" placeholder="New Trainer Name">
            </div>
            <button type="submit" class="btn btn-primary">Update Trainer</button>
        </form>
        <div style="padding-top: 10px">
            <button id="deleteTrainerButton" type="button" class="btn btn-danger">Delete Trainer</button>
        </div>
    </div>
    `
    document.getElementById("updateCardForm").addEventListener("submit", handleTrainerUpdate);
    document.getElementById("deleteTrainerButton").addEventListener("click", handleDeleteTrainer)
}

function trainerToRow(trainer) {
    return `
    <tr id="${getRowId(trainer)}" onclick="updateDetails('${trainer?.id?.trim()}')">
        <th scope="row">${trainer.id}</th>
        <td>${trainer.name}</td>
    </tr>
    `
}

function createTrainersTable() {
    const tableParent = document.getElementById(tableParentId);
    tableParent.innerHTML = `
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                </tr>
            </thead>
            <tbody>
                ${
        trainers.data.map(resource => `${trainerToRow(resource)}`).join("\n")
        || "no data"
    }
            </tbody>
        </table>
    `
}

function refreshTable() {
    tableParentId = "tableDiv";
    detailsParentId = "detailsDiv";

    fetch('/pokemon/api/trainers')
        .then(res => res.json())
        .then(data => {
            trainers = data;
            createTrainersTable();

            clearTrainerDetails();
            if (selectedDetails) {
                updateDetails(selectedTrainerID);
            }
        })
        .catch(err => {
            console.error(`Unable to fetch trainers: ${err.status}`);
            console.error(err);
        });
}


function handleTrainerCreate(event) {
    event.preventDefault();
    const trainerName = document.getElementById("trainerName").value;

    const postData = JSON.stringify({name: trainerName})


    fetch(
        'api/trainers', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: postData
        })
        .then(response => response.json())
        .then(data => {
            refreshTable();
        })
        .catch(err => {
            console.error('Error', err);
        })
}

function handleTrainerUpdate(event) {
    event.preventDefault();
    const trainerName = document.getElementById("updateTrainerInput").value;
    const putData = JSON.stringify({id: selectedTrainerID, name: trainerName});

    fetch(
        `api/trainers/${selectedTrainerID}`, {
            method: 'PUT',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: putData
        })
        .then(response => response.json())
        .then(data => {
            // clearTrainerDetails();
            refreshTable();
            // updateDetails(selectedTrainerID);
        })
        .catch(err => {
            console.error('Error', err);
        })
}

function handleDeleteTrainer(event) {
    event.preventDefault();

    fetch(
        `api/trainers/${selectedTrainerID}`, {
            method: 'DELETE'
        })
        .then(response => {
            selectedDetails = false;
            selectedTrainerID = null;

            refreshTable();
        })
        .catch(err => {
            console.error('Error', err);
        })
}